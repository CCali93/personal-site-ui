var gulp = require('gulp'),
	gutil = require('gulp-util'),
	jshint = require('gulp-jshint');

var concat = require('gulp-concat'),
	rename = require('gulp-rename'),
	uglify = require('gulp-uglify');

var concatCss = require('gulp-concat-css'),
	cssmin = require('gulp-cssmin');

var rimraf = require('gulp-rimraf');

gulp.task('copyFonts', function() {
	gulp.src('bower_components/Materialize/dist/font/*', {read: false}).pipe(gulp.dest('dist/font'));
});

gulp.task('copyHtml', function() {
	gulp.src('app/components/**/*.html', {read: false}).pipe(gulp.dest('dist/components'));
});

gulp.task('buildjs', function() {
	var dependencies = [
		'bower_components/jquery/dist/jquery.min.js',
		'bower_components/Materialize/dist/js/materialize.min.js',
		'bower_components/angular/angular.min.js',
		'bower_components/angular-ui-router/release/angular-ui-router.min.js',
		'bower_components/angular-timeline/dist/angular-timeline.js',
		'bower_components/moment/min/moment.min.js',
		'app/**/*.js'
	];

	gulp.src(dependencies, {read: false})
		.pipe(concat('app.js'))
		.pipe(gulp.dest('dist/js'))
		.pipe(rename('app.min.js'))
		.pipe(uglify())
		.pipe(gulp.dest('dist/js'));
});

gulp.task('buildCss', function() {
	var dependencies = [
		'bower_components/Materialize/dist/css/materialize.css',
		'bower_components/angular-timeline/dist/angular-timeline.css',
		'assets/css/**/*.css'
	];

	gulp.src(dependencies, {read: false})
		.pipe(concatCss('app.css'))
		.pipe(gulp.dest('dist/css'))
		.pipe(rename('app.min.css'))
		.pipe(cssmin())
		.pipe(gulp.dest('dist/css'));
});

gulp.task('build', ['buildjs', 'buildCss', 'copyHtml', 'copyFonts']);

// configure the jshint task
gulp.task('jshint', function() {
	return gulp.src('app/**/*.js')
		.pipe(jshint())
		.pipe(jshint.reporter('jshint-stylish'));
});

gulp.task('watch', function() {
	gulp.watch('app/**/*.js', ['jshint']);
});

gulp.task('default', function() {
	return gutil.log('Gulp is running!')
});
