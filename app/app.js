var personalSiteApp = angular.module('personalSiteApp', [
    'ui.router',
    'angular-timeline'
]);

personalSiteApp.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/');

    $stateProvider

        // HOME STATES AND NESTED VIEWS ========================================
        .state('home', {
            url: '/',
            views: {
                '': {
                    templateUrl: 'components/home/partial-home.html',
                    controller: 'HomeController'
                },
                'skills@home': {
                    templateUrl: 'components/home/skills/partial-skills.html',
                    controller: 'SkillsController'
                },
                'testimonials@home': {
                    templateUrl: 'components/home/testimonials/partial-testimonials.html',
                    controller: 'TestimonialsController'
                }
            }
        })

        // STATE FOR THE WORK EXPERIENCE PAGE ==================================
        .state('experience', {
            url: '/experience',
            templateUrl: 'components/experience/partial-experience.html',
            controller: 'ExperienceController'
        });
}]);
