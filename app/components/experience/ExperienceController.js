var personalSiteApp = angular.module('personalSiteApp');

personalSiteApp.controller('ExperienceController', ExperienceController);

ExperienceController.$inject = ['$scope'];

function ExperienceController($scope) {
    var now = new Date();

    $scope.events = [{
            badgeClass: 'blue darken-3',
            icon: 'grade',
            title: 'Internship at Krowdbyz',
            content: 'First foray into real software development',
            date: moment(now).format('MMM YYYY')
        }, {
            badgeClass: 'orange darken-4',
            icon: 'school',
            title: 'Began attending RIT',
            content: 'First step towards an amazing career',
            date: moment(now).format('MMM YYYY')
        }
    ];
}
