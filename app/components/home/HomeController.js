var personalSiteApp = angular.module('personalSiteApp');

personalSiteApp.controller('HomeController', HomeController);

HomeController.$inject = ['$scope'];

function HomeController($scope) {
    //TODO: add API calls
    $scope.title = 'Who I Am';

    $scope.elevatorPitch =
        'I am currently a 4th year Software Engineering Major at the '+
        'Rochester Institute of Technology (RIT). I have performed ' +
        'both front and back-end development for web-based systems. ' +
        'I take pride in creating exceptional software for people to ' +
        'use, enjoy, and add efficiency to their lives.';

    $scope.strengths = [
        {
            icon: 'flash_on',
            tagLine: 'Speeds up development',
            description: 'We did most of the heavy lifting for you to ' +
                'provide a default stylings that incorporate our ' +
                'custom components. Additionally, we refined ' +
                'animations and transitions to provide a smoother ' +
                'experience for developers.'
        },
        {
            icon: 'group',
            tagLine: 'User Experience Focused',
            description: 'By utilizing elements and principles of ' +
                'Material Design, we were able to create a framework ' +
                'that incorporates components and animations that ' +
                'provide more feedback to users. Additionally, a single ' +
                 'underlying responsive system across all platforms ' +
                 'allow for a more unified user experience.'
        },
        {
            icon: 'settings',
            tagLine: 'Easy to work with',
            description: 'We have provided detailed documentation' +
                'as well as specific code examples to help new users' +
                'get started. We are also always open to feedback and' +
                'can answer any questions a user may have about' +
                'Materialize.'
        }
    ];
}
