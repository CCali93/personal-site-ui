var personalSiteApp = angular.module('personalSiteApp');

personalSiteApp.controller('TestimonialsController', TestimonialsController);

TestimonialsController.$inject = ['$scope'];

function TestimonialsController($scope) {
    $scope.testimonials = [
        {
            quote: 'I worked with Curtis during his summer internship at Cisco. ' +
                'Curtis is a quick learner and can understand technical ' +
                'intricacies without much effort. He was able to quickly get a ' +
                'hold on the product codebase and was able to modify it with ' +
                'the required additions. He worked mostly on the UI side of the ' +
                'product and came in handy with good programming skills...',
            person: 'Danish Chopra',
            email: 'dachopra@cisco.com'
        },
        {
            quote: 'He\'s a great person to work with.',
            person: 'John Doe',
            email: 'jd@gmail.com'
        },
        {
            quote: 'He\'s a great person to work with.',
            person: 'John Doe',
            email: 'jd@gmail.com'
        }
    ];
}
